# survey

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/survey/00_README)

The FSFE's installation of LimeSurvey which is using an image of [acspri/limesurvey](https://hub.docker.com/r/acspri/limesurvey) and official [MariaDB](https://hub.docker.com/_/mariadb) image. 


## Environment variables

Required environment variables:

- LIMESURVEY_ADMIN_USER = Username for limesurvey administrator
- LIMESURVEY_ADMIN_PASSWORD = Password for limesurvey administrator
- LIMESURVEY_ADMIN_NAME =  Name for limesurvey administrator
- LIMESURVEY_ADMIN_EMAIL = Email for limesurvey administrator
- LIMESURVEY_DB_NAME = Name for database
- LIMESURVEY_TABLE_PREFIX = Prefix for database tables
- LIMESURVEY_DB_PASSWORD =  Password for database


## Update Strategie

We use [Renovate](https://docs.fsfe.org/en/repodocs/renovate/renovate) to update our LimeSurvey instance. If there is a suggestion in the web interface about an available update, please do not pay attention to it, as we could currently using a more stable version of LimeSurvey, and not the latest one. Additionally, the `ComfortUpdate` tool suggested in the web interface is a paid tool offered by LimeSurvey, so consider it as an advertisement. Thanks to [Renovate](https://docs.fsfe.org/en/repodocs/renovate/renovate) and our dedicated sysadmins, our LimeSurvey instance is always up to date with its most stable version.